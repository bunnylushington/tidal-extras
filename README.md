# tidal-extras

Some completion for [tidal-mode](https://tidalcycles.org/docs/getting-started/editor/Emacs/) in Emacs.

### Installation

I use

``` emacs-lisp
(use-package tidal-extras
  :straight '(tidal-extras
              :type git
              :host codeberg
              :repo "bunnylushington/tidal-extras")
  :after tidal
  :hook (tidal-mode . (lambda ()
                        (add-hook 'completion-at-point-functions
                                  #'tidal-extras/completion-at-point nil t))))

```


### License

MIT (see included LICENSE.txt)

### Author

Bunny Lushington; September 2023
